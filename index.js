/*
Теоретичне питання:

1.Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

1.Асинхронність у JavaScript - це можливість виконання деяких операцій без очікування завершення інших.
*/

// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const btn = document.querySelector(".btn");
const URL_IP = "https://api.ipify.org/?format=json";


btn.addEventListener('click', async () => {

    try {
        const resIp = await fetch(URL_IP);
        const ipData = await resIp.json();
        const ipAdress = ipData.ip;
        console.log(ipAdress);

        const URL_LOCATION = `http://ip-api.com/json/${ipAdress}?fields=1699839`;
        const resLocation = await fetch(URL_LOCATION);
        const locationData = await resLocation.json();

         console.log(locationData);

        const { continent, country, regionName, city } = locationData;

        const locationInfo = `Континент: ${continent}<br>Країна: ${country}<br>Регіон: ${regionName}<br>Місто: ${city}`;
        const locationElement = document.createElement('div');
        locationElement.innerHTML = locationInfo;
        document.body.appendChild(locationElement);
    } catch (error) {
        console.log(error)
    }

});

